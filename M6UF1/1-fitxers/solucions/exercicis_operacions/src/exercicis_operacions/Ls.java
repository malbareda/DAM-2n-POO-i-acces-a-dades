package exercicis_operacions;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Ls {
	public static void main(String[] args) {
		if (args.length>0) {
			String directori = args[0];
			Path dir = Paths.get(directori);
			if (Files.isDirectory(dir)) {
				System.out.println("Fitxers del directori "+directori);
				try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
					for (Path f: stream) {
						System.out.print(Files.isDirectory(f)?"d":"-");
						System.out.print(Files.isReadable(f)?"r":"-");
						System.out.print(Files.isWritable(f)?"w":"-");
						System.out.print(Files.isExecutable(f)?"x":"-");
						System.out.println(" "+f.getFileName());
					}
				} catch (IOException | DirectoryIteratorException ex) {		    
					System.err.println(ex);
				}
			} else
				System.err.println("No és un directori");
		} else
			System.err.println("S'esperava un nom de directori");
	}

}
