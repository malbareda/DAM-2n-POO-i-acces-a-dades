## El domino

En aquest exercici implementarem un programa per jugar al domino.

Es proporciona una
part important del [codi](../codi/dominoEnunciat) ja desenvolupat. Cal localitzar els
diversos comentaris marcats amb ***TODO*** en codi i completar-los seguint les
seves instruccions.

Hi ha les següents parts per completar de cada classe:

- Classe Fitxa. 3 TODOs.
- Classe Domino. 1 TODO.
- Classe Jugador. 2 TODOs.
- Classe Taula. 2 TODOs.
- Classe Partida. 3 TODOs.
- Classe Principal. 1 TODO.

A continuació podeu veure un exemple de l'execució del programa un cop
completat:

```
Torn del jugador 1
Fitxes jugades:
Fitxes a la mà: [0|0] [0|1] [5|5] [0|5] [3|4] [1|4] [4|5]
Quina fitxa vols jugar?
5 5
Fitxa jugada.

Torn del jugador 2
Fitxes jugades: [5|5]
Fitxes a la mà: [2|6] [0|3] [3|3] [3|5] [1|2] [3|6] [4|6]
Quina fitxa vols jugar?
3 5
A quina banda vols jugar? (d/e)
d
Fitxa jugada.

Torn del jugador 1
Fitxes jugades: [5|5][5|3]
Fitxes a la mà: [0|0] [0|1] [0|5] [3|4] [1|4] [4|5]
Quina fitxa vols jugar?
4 5
Fitxa jugada.
...
Torn del jugador 2
Fitxes jugades: [0|6][6|3][3|4][4|5][5|5][5|3][3|3][3|0][0|0]
Fitxes a la mà: [2|6] [1|2] [6|6] [1|6] [4|4] [4|6] [2|2]
No pots jugar cap fitxa.

Torn del jugador 2
Fitxes jugades: [0|6][6|3][3|4][4|5][5|5][5|3][3|3][3|0][0|0]
Fitxes a la mà: [2|6] [0|4] [1|2] [6|6] [1|6] [4|4] [4|6] [2|2]
Quina fitxa vols jugar?
0 4
A quina banda vols jugar? (d/e)
d
Fitxa jugada.
...
Torn del jugador 1
Fitxes jugades: [1|5][5|0][0|6][6|3][3|4][4|5][5|5][5|3][3|3][3|0][0|0][0|4][4|1][1|6][6|6][6|4]
Fitxes a la mà: [0|1]
Quina fitxa vols jugar?
0 1
Fitxa jugada.

S'ha acabat la partida
Puntuacions (com menys millor):
Jugador 1 no té cap fitxa
Jugador 2 té les fitxes [2|6] [1|2] [4|4] [2|2] amb puntuació 23
```
