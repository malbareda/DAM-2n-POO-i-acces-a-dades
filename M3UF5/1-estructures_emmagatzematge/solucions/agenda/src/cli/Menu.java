package cli;

import java.util.*;

import agenda.Agenda;
import agenda.Contacte;

public class Menu {
	private Agenda agenda = new Agenda();
	private Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		new Menu().menu();
	}
	
	public void menu() {
		int opcio = 0;
		
		while (opcio != 5) {
			System.out.println("1- Afegir contacte");
			System.out.println("2- Esborrar contacte");
			System.out.println("3- Modificar contacte");
			System.out.println("4- Cercar contacte");
			System.out.println("5- Sortir");
			System.out.println("\nIntrodueix l'opció desitjada: ");
			try {
				opcio = scanner.nextInt();
				scanner.nextLine();
			} catch (InputMismatchException e) {
				opcio = -1;
			}
			
			switch (opcio) {
			case 1:
				afegeix();
				break;
			case 2:
				esborra();
				break;
			case 3:
				modifica();
				break;
			case 4:
				cerca();
				break;
			case 5:
				System.out.println("Fins la propera!");
				break;
			default:
				System.out.println("Opció incorrecta");
				break;
			}
		}
	}
	
	public void afegeix() {
		String nom, cognoms, telefon, adreca;
		
		System.out.println("Introdueix el nom del contacte: ");
		nom = scanner.nextLine();
		System.out.println("Introdueix els cognoms del contacte: ");
		cognoms = scanner.nextLine();
		System.out.println("Introdueix el telèfon del contacte: ");
		telefon = scanner.nextLine();
		System.out.println("Introdueix l'adreça del contacte: ");
		adreca = scanner.nextLine();
		
		Contacte c = new Contacte(nom, cognoms, adreca, telefon);
		try {
			agenda.afegeix(c);
			System.out.println("El contacte s'ha afegit.");
		} catch (IllegalArgumentException e) {
			System.out.println("El contacte ja existeix a l'agenda.");
		}
	}
	
	public void esborra() {
		String nom, cognoms;
		
		System.out.println("Introdueix el nom del contacte: ");
		nom = scanner.nextLine();
		System.out.println("Introdueix els cognoms del contacte: ");
		cognoms = scanner.nextLine();
		
		if (agenda.elimina(nom, cognoms) == true)
			System.out.println("El contacte s'ha esborrat.");
		else
			System.out.println("No s'ha trobat el contacte.");
		
	}
	
	public void modifica() {
		String nom, cognoms;
		String nouNom, nousCognoms, novaAdreca, nouTelefon;
		Contacte vellContacte;
		
		System.out.println("Introdueix el nom del contacte a modificar: ");
		nom = scanner.nextLine();
		System.out.println("Introdueix els cognoms del contacte a modificar: ");
		cognoms = scanner.nextLine();
		
		vellContacte = agenda.cerca(nom,  cognoms);
		if (vellContacte != null) {
			System.out.println("Introdueix el nou nom del contacte (en blanc per mantenir): ");
			nouNom = scanner.nextLine();
			if (nouNom.equals(""))
				nouNom = vellContacte.getNom();
			System.out.println("Introdueix els nous cognoms del contacte (en blanc per mantenir): ");
			nousCognoms = scanner.nextLine();
			if (nousCognoms.equals(""))
				nousCognoms = vellContacte.getCognoms();
			System.out.println("Introdueix el nou telèfon del contacte (en blanc per mantenir): ");
			nouTelefon = scanner.nextLine();
			if (nouTelefon.equals(""))
				nouTelefon = vellContacte.getTelefon();
			System.out.println("Introdueix la nova adreça del contacte (en blanc per mantenir): ");
			novaAdreca = scanner.nextLine();
			if (novaAdreca.equals(""))
				novaAdreca = vellContacte.getAdreca();
			
			Contacte nouContacte = new Contacte(nouNom, nousCognoms, novaAdreca, nouTelefon);
			agenda.modifica(nom, cognoms, nouContacte);
		} else {
			System.out.println("El contacte no existeix.");
		}
	}
	
	public void cerca() {
		String nom, cognoms, adreca, telefon;
		
		System.out.println("Introdueix el nom del contacte (en blanc per no cercar per nom): ");
		nom = scanner.nextLine();
		System.out.println("Introdueix els cognoms del contacte (en blanc per no cercar per cognom): ");
		cognoms = scanner.nextLine();
		System.out.println("Introdueix el telèfon del contacte (en blanc per no cercar per telèfon): ");
		telefon = scanner.nextLine();
		System.out.println("Introdueix l'adreça del contacte (en blanc per no cercar per adreça): ");
		adreca = scanner.nextLine();
		
		Contacte criteri = new Contacte(nom, cognoms, adreca, telefon);
		List<Contacte> resultat = agenda.cerca(criteri);
		if (resultat.size() > 0) {
			System.out.println("Els següents contactes compleixen els criteris:");
			for (Contacte c : resultat) {
				System.out.println("Nom: "+c.getNom()+"  Cognoms: "+c.getCognoms());
				System.out.println("Adreça: "+c.getAdreca());
				System.out.println("Telèfon: "+c.getTelefon());
				System.out.println();
			}
		} else {
			System.out.println("No hi ha cap contacte que compleixi aquests criteris.");
		}
	}

}
