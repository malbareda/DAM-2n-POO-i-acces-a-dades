package base;

public class BaseFina implements Ingredient, Cloneable {

	@Override
	public String recepta() {
		return "El cuiner estén una finíssima base de pizza sobre el marbre.";
	}

	@Override
	public double getPreu() {
		return 6.25;
	}

	@Override
	public String descripcio() {
		return "una base de pizza fina";
	}
	@Override
	public BaseFina clone() throws CloneNotSupportedException {
		return (BaseFina) super.clone();
	}
}
