package piles;

public class PilaNode implements Pila {
	private Node top;

	/* (non-Javadoc)
	 * @see piles.Pila#push(int)
	 */
	@Override
	public void push(int n) {
		Node nouNode = new Node();
		nouNode.setN(n);
		nouNode.setPrev(top);
		top = nouNode;
	}

	/* (non-Javadoc)
	 * @see piles.Pila#pop()
	 */
	@Override
	public int pop() throws PilaBuidaException {
		if (isEmpty()) {
			throw new PilaBuidaException("La pila és buida.");
		}
		int retorn = top.getN();
		top = top.getPrev();
		return retorn;
	}

	/* (non-Javadoc)
	 * @see piles.Pila#peek()
	 */
	@Override
	public int peek() throws PilaBuidaException {
		if (isEmpty()) {
			throw new PilaBuidaException("La pila és buida.");
		}
		return top.getN();
	}

	/* (non-Javadoc)
	 * @see piles.Pîla#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return top==null;
	}
	
	private class Node {
		private int n;
		private Node prev;
		
		public int getN() {
			return n;
		}
		public void setN(int n) {
			this.n = n;
		}
		public Node getPrev() {
			return prev;
		}
		public void setPrev(Node prev) {
			this.prev = prev;
		}
	}
}
