package piles;

public interface Pila {

	void push(int n);

	int pop() throws PilaBuidaException;

	int peek() throws PilaBuidaException;

	boolean isEmpty();

}