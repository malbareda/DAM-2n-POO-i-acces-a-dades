package piles;

public class PilaArray implements Pila {
	private int[] array = new int[10];
	private int mida = 0;
	
	private void duplicaArray() {
		int[] nouArray = new int[array.length*2];
		for (int i=0; i<array.length; i++) {
			nouArray[i] = array[i];
		}
		array = nouArray;
	}
	
	/* (non-Javadoc)
	 * @see piles.Pila#push(int)
	 */
	@Override
	public void push(int n) {
		if (mida==array.length) {
			duplicaArray();
		}
		array[mida] = n;
		mida++;
	}

	/* (non-Javadoc)
	 * @see piles.Pila#pop()
	 */
	@Override
	public int pop() throws PilaBuidaException {
		if (isEmpty()) {
			throw new PilaBuidaException("La pila és buida.");
		}
		mida--;
		return array[mida];
	}

	/* (non-Javadoc)
	 * @see piles.Pila#peek()
	 */
	@Override
	public int peek() throws PilaBuidaException {
		if (isEmpty()) {
			throw new PilaBuidaException("La pila és buida.");
		}
		return array[mida-1];
	}

	/* (non-Javadoc)
	 * @see piles.Pila#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return mida<=0;
	}
	
}

