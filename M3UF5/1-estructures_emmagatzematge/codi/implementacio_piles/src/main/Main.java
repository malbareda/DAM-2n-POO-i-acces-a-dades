package main;

import piles.PilaNode;
import piles.Pila;
import piles.PilaArray;
import piles.PilaBuidaException;

public class Main {

	public static void main(String[] args) {
		System.out.println("Comprovant PilaNode...");
		Pila p = new PilaNode();
		comprovaPila(p);
		System.out.println("Comprovant PilaArray...");
		p = new PilaArray();
		comprovaPila(p);
	}
	
	public static void comprovaPila(Pila p) {
		for (int i=0; i<1000; i++) {
			p.push(i);
		}
		
		try {
			while (!p.isEmpty()) {
				System.out.println(p.pop());
			}
		} catch (PilaBuidaException e) {
			System.out.println("fail");
		}
		
		try {
			p.pop();
			System.out.println("fail");
		} catch (PilaBuidaException e) {
			System.out.println("ok");
		}
	}
	
	public static void buidaPila(Pila p) {
		try {
			while (!p.isEmpty()) {
				System.out.println(p.pop());
			}
		} catch (PilaBuidaException e) {
			System.out.println("fail");
		}
	}

}
