package exemple_iterator;

public interface ElMeuIterator {
	public boolean hasNext();
	public int next();
}
