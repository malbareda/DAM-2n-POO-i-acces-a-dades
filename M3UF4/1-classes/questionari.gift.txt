::Polimorfisme::[markdown]
Donat el següent diagrama de classes, on *Vehicle*, *Animal* i *Ocell* són
classes abstractes, *Volador* és una interfície, i *Avio* i *Oreneta* són
classes concretes:
\n![Diagrama pregunta polimorfisme](https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades/raw/master/M3UF4/1-classes/imatges/questionari.png)
\nA més d'*Object*, de quins tipus és un objecte creat amb la sentència
*Ocell o = new Oreneta();*
{
  ~*Ocell* i *Oreneta*.
  ~*Ocell*, *Animal* i *Volador*.
  ~*Oreneta*.
  =*Ocell*, *Animal*, *Volador* i *Oreneta*.
}

::Quin mètode s'executa?::[markdown]
Donat el següent diagrama de classes:
\n![Diagrama pregunta quin mètode](https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades/raw/master/M3UF4/1-classes/imatges/questionari_001.png)
\nDesprés de declarar una variable com *Interficie i = new ClasseDerivada();*
\nQuin dels mètodes s'executa quan fem *i.metode();* ?
{
  ~El d'*Interficie*, perquè és el tipus de la variable.
  ~El de *ClasseBase*, perquè és el més general.
  =El de *ClasseDerivada*, perquè és el tipus de l'objecte.
  ~Cap d'ells, es produeix un error de compilació, perquè les interfícies no poden contenir codi.
}

::Classes abstractes::[markdown]
Si *Animal* és una classe abstracte... {
  ~no pot existir cap objecte que sigui de tipus *Animal*.
  ~no pot existir cap variable que s'hagi declarat de tipus *Animal*.
  =no es pot instanciar un objecte com a tipus *Animal*.
  ~el tipus *Animal* no pot tenir cap mètode constructor.
}

::instanceof i disseny::[markdown]
Donada la següent estructura de classes:
\n![Diagrama pregunta instanceof i disseny](https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades/raw/master/M3UF4/1-classes/imatges/questionari_002.png)
\nEl codi del mètode *alimenta()* és similar a\:\n
\n    public void alimenta(Animal a) \{
\n        ...
\n        if (a instanceof Serp) \{
\n            ...
\n        \} else \{
\n            ...
\n        \}
\n    \}
{
  ~El disseny del programa és correcte, perquè tenim el codi per alimentar centralitzat a la classe *Zoo*.
  ~Hauríem de crear el mètode alimenta a *Serp*, i fer que *Zoo.alimenta()* el cridi si a és una *Serp*.
  =Hauríem de crear el mètode alimenta a *Animal*, sobreescriure'l a *Serp*, i que *Zoo.alimenta()* cridi sempre *a.alimenta()*.
  ~Hauríem de crear el mètode alimenta a *Animal*, amb el mateix codi que hi ha ara, i que *Zoo.alimenta()* cridi sempre *a.alimenta()*.
}

::Referències i tipus::[markdown]
En aquesta estructura de classes:
\n![Diagrama pregunta referències i tipus](https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades/raw/master/M3UF4/1-classes/imatges/questionari_003.png)
\nQuines de les següents sentències són vàlides?\n
\n    Estudiant e = new Persona();
\n    Persona p = new Estudiant();
{
  ~La primera és vàlida, però la segona no.
  =La primera no és vàlida, però la segona sí.
  ~Les dues són vàlides.
  ~Cap de les dues és vàlida.
}

::equals::[markdown]
Pel que fa a l'operador == i al mètode *equals()*...
{
  ~Són sempre equivalents.
  ~Són sempre equivalents, excepte en el cas de la classe *String*.
  =Són equivalents, excepte per les classes en què s'hagi sobreescrit *equals()*.
  ~L'operador \=\= comprova si dues referències apunten al mateix objecte, mentre que *equals()* comprova que dos objectes siguin del mateix tipus i tinguin el mateix valor als seus atributs.
}

::super::[markdown]
Donada la següent estructura:
\n![Diagrama pregunta super](https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades/raw/master/M3UF4/1-classes/imatges/questionari_004.png)
\nQuina de les següents afirmacions **NO** és correcta? {
  ~Des de *Bicicleta* podem cridar el mètode *accelera()* de *Vehicle* fent *super.accelera()*.
  =Des de *Bicicleta* podem cridar el mètode *accelera()* de *Vehicle* fent *this.accelera()*.
  ~Des de *Cotxe* podem cridar el mètode *accelera()* de *Vehicle* fent *accelera()* o *this.accelera()*.
  ~Des de *Cotxe* podem cridar el mètode *accelera()* de *Vehicle* fent *super.accelera()*.
}

::interfícies::Una interfície pot incloure declaracions de mètodes, ... {
  =atributs constants i implementacions per defecte de mètodes.
  ~i atributs constants.
  ~atributs qualssevol i implementacions per defecte de mètodes.
  ~mètodes final i atributs qualssevol.
}

::Herència de constructors::[markdown]
Quina serà la sortida del següent programa?\n
\n    public class Vehicle \{
\n	      public Vehicle() \{
\n		        System.out.print("S'ha creat un vehicle.");
\n	      \}
\n    \}
\n    public class Cotxe extends Vehicle \{
\n	      public Cotxe() \{
\n		        System.out.print("S'ha creat un cotxe.");
\n	      \}
\n    \}
\n    public class Ambulancia extends Cotxe \{
\n	      public Ambulancia() \{
\n		        System.out.print("S'ha creat una ambulància.");
\n	      \}
\n    \}
\n    public class Programa \{
\n	      public static void main(String args[]) \{
\n		        Vehicle v \= new Ambulancia();
\n	      \}
\n    \}\n
{
  ~S'ha creat una ambulància. S'ha creat un cotxe. S'ha creat un vehicle.
  =S'ha creat un vehicle. S'ha creat un cotxe. S'ha creat una ambulància.
  ~S'ha creat una ambulància.
  ~S'ha creat un vehicle.
}

::abstract::Pel que fa a mètodes i classes abstractes, quina d'aquestes afirmacions és certa?
{
  ~Una classe abstracte ha de tenir obligatòriament un o més mètodes abstractes.
  ~Una classe abstracte només pot contenir mètodes abstractes.
  =Una classe que té un mètode abstracte ha de ser obligatòriament abstracte.
  ~Una classe que deriva d'una classe abstracte ha d'implementar obligatòriament totes les operacions abstractes que hereti.
}
