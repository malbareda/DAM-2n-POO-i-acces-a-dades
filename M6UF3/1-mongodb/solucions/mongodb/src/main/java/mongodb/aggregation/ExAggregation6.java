package mongodb.aggregation;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.descending;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExAggregation6 {
	private static Integer millorClasse;

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("grades");

		coll.aggregate(Arrays.asList(
			unwind("$scores"),
			match(eq("scores.type", "exam")),
			group("$class_id", avg("score", "$scores.score")),
			sort(descending("score"))
		)).forEach((Document doc) -> {
			if (millorClasse==null)
				millorClasse = doc.getInteger("_id");
			System.out.format("Classe: %d, mitjana exàmens: %.2f.\n",
					doc.getInteger("_id"), doc.getDouble("score"));
		});
		System.out.println("La millor classe és la "+millorClasse+".");
		
		client.close();
	}

}
