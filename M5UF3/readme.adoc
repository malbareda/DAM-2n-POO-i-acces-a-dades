= Introducció al disseny orientat a objectes
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Unified Modeling Language (UML)

L'UML és un llenguatge de modelat que intenta abastar totes les fases d'anàlisi,
disseny i desenvolupament d'una aplicació, especialment en els casos orientats
a objectes.

L'UML utilitza una sèrie de diagrames de diversos tipus que ofereixen
una gran flexibilitat. L'última versió de l'UML és la 2.5, de juny de 2015.

Hi ha dos grans grups de diagrames, els estructurals, que representen aspectes
estàtics, i els de comportament, que representen relacions dinàmiques, com
interaccions o comunicació entre elements. En total hi ha 7 tipus de diagrames
estructurals i 7 tipus més de diagrames de comportament.

Aquí veurem només els diagrames més utilitzats a la pràctica.

== Diagrames de classes

Els **diagrames de classes** són els diagrames UML més utilitzats, i els que
permeten un rang més ampli de conceptes de modelat. Un diagrama de
classes descriu els tipus d'objecte que apareixen en un sistema i les
diverses relacions estàtiques que hi ha entre ells. També s'hi mostren
les propietats i operacions de les classes i les restriccions que
s'apliquen a la forma com es connecten els objectes.

Els diagrames de classes són similars als diagrames entitat-relació, però
amb les següents diferències:

* Els diagrames E-R modelen només dades. En canvi els diagrames de classes
modelen tant els atributs com els mètodes de les classes. En general, de fet,
l'atenció se centra més en els mètodes que no pas en les dades.

* Els diagrames de classes permeten alguns elements que els E-R no permeten.
Per exemple, el concepte d'interfície o classe abstracte no té sentit en un
diagrama E-R.

* Els diagrames de classes poden mostrar relacions entre classes que no es
poden mostrar en E-R. Per exemple, el fet que un mètode d'una classe rebi
com a paràmetre un objecte d'una altra classe, i per tant, en depengui, és
quelcom que no es pot expressar en E-R. Això té sentit, ja que es tracta d'una
relació dinàmica i no quedaria plasmada en una base de dades.

=== Classes i interfícies

Les classes i interfícies es representen amb una caixa que conté el seu nom.
Opcionalment poden aparèixer els seus atributs i mètodes.

Exemple de classe, classe abstracte i interfície:

[link=uml/exemple_classe.puml]
image::imatges/exemple_classe.png[Exemple de classe]

Cal notar que en els diagrames de classes la major part dels elements són
opcionals. Així, poden aparèixer o no els mètodes i atributs, o només una part
d'ells. També es poden incloure o no els modificadors d'accés o els tipus de
paràmetres i retorn.

En general, l'objectiu d'un diagrama de classes és representar la idea d'un
disseny orientat a objectes i es destaquen només aquells elements que
interessen. En poques ocasions es creen diagrames que representin tots els
elements d'un programa.

=== Característiques

Utilitzem el terme **característica** (*feature*) com un terme general que
engloba tant les propietats com les operacions d'una classe.

==== Propietats

Les **propietats** representen característiques estructurals d'una classe.
En general, les propietats tindran una equivalència més o menys directa
amb els atributs de la classe. Les propietats poden aparèixer en un
diagrama de dues formes completament diferents: com a *atributs* o com a
*associacions*.

Les propietats són quelcom que un objecte sempre pot proveir, i per
tant, no s'han d'utilitzar per modelar una relació temporal, com un
objecte que es passa com a paràmetre en la crida d'un mètode i que
s'utilitza només dins dels límits d'aquesta interacció.

==== Atributs

La notació en forma d'**atribut** descriu una propietat com una línia de
text dins de la caixa de la classe. La forma completa d'un atribut és:

`visibilitat nom: tipus [multiplicitat] = defecte {etiquetes}`

`- nom: String [1] = "Sense títol" {readOnly}`

==== Associacions

Una **associació** es representa com una línia contínua entre dues classes,
dirigida de la classe font a la classe destí. Es pot incloure el nom de la
propietat a l'extrem del destí, i també la seva multiplicitat.
L'extrem objectiu enllaça amb la classe que es correspon al
tipus de la propietat.

==== Atributs o associacions?

Atributs i associacions són dues formes diferents d'expressar les mateixes
propietats d'una classe. Per exemple, els dos diagrames següents mostren
gairebé la mateixa informació de les dues formes:

[link=uml/atribut_associacio.puml]
image::imatges/atribut_associacio.png[Com atributs]

[link=uml/atribut_associacio.puml]
image::imatges/atribut_associacio_001.png[Com associacions]

En el segon diagrama, menys compacte, podem veure alguna informació de més.
Per exemple, veiem que un nom només pot estar assignat a un personatge,
és a dir, que no hi poden haver dos personatges amb el mateix nom, o que
un ítem pot pertànyer o no a un personatge, però que una mascota ha de
pertànyer sempre a un personatge.

En general utilitzarem la notació en forma d'atribut per tipus primitius o
classes de les API de Java, i la notació com associació pels casos en què
també volem representar informació de la classe destí.

==== Interpretació de les propietats

Una propietat es pot interpretar habitualment com un atribut, però també com
al conjunt de l'atribut i els seus mètodes *get* i *set*. Així, per no
sobrecarregar un diagrama amb informació, és habitual no posar els mètodes
d'accés a un atribut i, si ambdós existeixen i són públics, posar directament
la visibilitat de la propietat com a pública, encara que realment sigui
privada a la implementació.

També és possible que existeixin mètodes *get* i *set* no associats a un
atribut. Per exemple, una figura geomètrica pot tenir un mètode *getArea()*
o una persona un mètode *getNomComplet()* i que realment no existeixi un
atribut *àrea*, sinó que es calculi en funció dels vèrtexs de la figura,
i que no existeixi un atribut *nomComplet*, sinó que s'obtingui a partir
de concatenar el nom i els cognoms.

===== Propietats que no són atributs

Imaginem per exemple que volem implementar la classe rectangle. L'àrea del
rectangle es calcula a partir de la seva amplada i alçada, i podem optar per
guardar-la a un atribut o no.

En aquesta primera implementació, l'àrea és un atribut i es recalcula cada cop
que es modifica l'alçada o amplada del rectangle:

[source,java]
----
public class Rectangle {
	private double width, height;
	private double area;

	public Rectangle(double width, double height) {
		this.width = width;
		this.height = height;
		area = width*height;
	}

	public double getWidth() {
		return width;
	}
	public double getArea() {
		return area;
	}
	// Altres getters

	public double setWidth(double width) throws IllegalArgumentException {
		if (width<0)
			throw new IllegalArgumentException("Width cannot be negative.");
		this.width = width;
		area = width*height;
	}
	public double setHeight(double height) throws IllegalArgumentException {
		if (height<0)
			throw new IllegalArgumentException("Height cannot be negative.");
		this.height = height;
		area = width*height;
	}
}
----

Aquesta primera implementació és útil si l'àrea es necessita moltes vegades,
però l'alçada i amplada dels rectangles no es modifica gaire. D'aquesta manera
evitem repetir el mateix càlcul molts cops (podem imaginar un exemple més
complex on el càlcul de l'àrea sigui realment costós).

Fem ara una segona implementació en què l'àrea es calcula cada cop que es
necessita, i no es guarda en un atribut:

[source,java]
----
public class Rectangle {
	private double width, height;

	public double getWidth() {
		return width;
	}
	// Altres getters i setters

	public double getArea() {
		return width*height;
	}
}
----

Aquesta implementació es podria utilitzar en els casos en què no es necessiti
consultar l'àrea moltes vegades, o en què l'amplada i alçada canviessin molt
més sovint que no pas es necessités consultar l'àrea.

Finalment, una tercera possibilitat seria aquesta:

[source,java]
----
public class Rectangle {
	private double width, height;
	private double area = -1;

	public double getWidth() {
		return width;
	}
	public double getArea() {
		if (area<0) {
			area = width*height;
		}
		return area;
	}
	// Altres getters

	public double setWidth(double width) throws IllegalArgumentException {
		if (width<0)
			throw new IllegalArgumentException("Width cannot be negative.");
		this.width = width;
		area = -1;
	}
	// Altres setters
}
----

Aquesta darrera possibilitat minimitza la quantitat de vegades en què es
recalcula l'àrea. Si es fan diverses modificacions a l'amplada o alçada del
rectangle seguides, no es calcula l'àrea en cada ocasió. Si es demana l'àrea
diversos cops seguits, només es calcula la primera vegada que es necessita.

La part interessant d'aquests exemples és que la interfície pública de la
classe `Rectangle` és la mateixa independentment de la implementació triada.
Qualsevol programador que utilitzi la nostra classe ho farà de la mateixa
manera en els tres casos, i obtindrà exactament els mateixos resultats. Per
tant, és lògic pensar que el diagrama de classes hauria de ser el mateix en
els tres casos, i que l'`àrea` serà una propietat de `Rectangle` independentment
que s'hagi optat per utilitzar un atribut o no per emmagatzemar-la.

L'únic cas en què el diagrama seria diferent per cas seria si justament el que
volem representar són els detalls d'implementació de `Rectangle`.

El diagrama de `Rectangle` quedaria així:

[link=uml/exemple_rectangle.puml]
image::imatges/exemple_rectangle.png[Diagrama classe Rectangle]

===== Propietats multivaluades

Un altre aspecte a tenir en compte en la interpretació de les propietats és el
que fa referència a les propietats multivaluades.

Si una propietat és multivaluada, implica que les dades s'estan
emmagatzemant en una col·lecció (habitualment una llista si l'ordre és
important, o un conjunt si no hi ha ordre lògic ni repeticions). En aquest
cas, la interfície que s'obté per a la propietat és diferent: no és només
un mètode *get* i un mètode *set*, sinó que hi poden haver una colla de mètodes
d'accés a la col·lecció.

Com que podem expressar les col·leccions fàcilment com a propietats
multivaluades, gairebé mai s'inclouen referències concretes a les
col·leccions en els diagrames.

A l'exemple anterior sobre el personatge (`Character`) i els objectes que té
(`Item`) es pot veure aquesta situació: els ítems d'inventari del personatge
es guardaran en un array (possiblement amb un atribut auxiliar que indiqui la
quantitat d'objectes que porta), o en una llista (per exemple, un `ArrayList`),
o en un conjunt (per exemple, en un `HashSet`).

Qualsevol d'aquestes implementacions internes queda descrita només indicant que
la cardinalitat de `inventory` és molts.

===== Associacions bidireccionals

Una associació bidireccional és un parell de propietats enllaçades com a
inverses. L'enllaç invers implica que si seguim ambdues propietats, hem
de retornar a un conjunt que contingui el punt inicial.

Quan es programen, cal assegurar que les dues propietats es mantenen
sincronitzades.

Per exemple, imaginem que volem que la relació entre un personatge (`Character`)
i la seva mascota (`Pet`) sigui bidireccional.

Això ens pot interessar si en algunes situacions ens convé recuperar la mascota
a partir del personatge, però en altres situacions necessitem recuperar el
personatge a partir de la mascota.

Si no hem fet que la relació sigui bidireccional, ens trobarem que l'única forma
de recuperar un personatge si tenim una referència a la seva mascota serà
recorrent tots els personatges i comprovant per a cadascun d'ells si la seva
mascota és o no la mascota que tenim:

[source,java]
----
Pet pet=...;
List<Character> characters=...;

for (Character c : characters) {
	if (c.getPet()==pet) {
		// Hem trobat el personatge associat a la mascota
	}
}
----

Si en el nostre programa hi ha molts personatges, aquesta forma de procedir
serà ineficient.

En canvi, si la relació és bidireccional en tindrem prou amb:

[source,java]
----
Pet pet=...;
Character c = pet.getOwner();
----

El problema amb aquest enfoc és que cal mantenir les dues propietats
sincronitzades:

[source,java]
----
public class Character {
	private Pet pet;

	public Pet getPet() {
		return pet;
	}
	protected setPet(Pet pet) {
		this.pet = pet;
	}
}

public class Pet {
	private Character owner;

	public Character getOwner() {
		return owner;
	}
	public void setOwner(Character newOwner) {
		if (owner != null) {
			owner.setPet(null);
		}
		this.owner = newOwner;
		newOwner.setPet(this);
	}
}
----

En aquest exemple hem de fixar-nos en la implementació de `Pet.setOwner()`.
Aquí és on hi ha la complexitat d'assegurar que les dues relacions no queden
dessincronitzades: per una banda eliminem la mascota del propietari anterior
(si n'hi havia), i per l'altra assignem el nou propietari.

Un cop fet això, és important que qualsevol part del codi que necessiti
canviar un propietari ho faci utilitzant `Pet.setOwner()` i no
`Character.setPet()`. Per evitar aquest error, hem fet que aquest últim
mètode sigui `protected`, i posarem les dues classes al mateix paquet.

Les associacions bidireccionals es representen amb un línia que acaba en
punta cap a les dues bandes.

[NOTE]
====
Si una associació no acaba en punta en cap dels dos costats, simplement
significa que no s'ha especificat en el diagrama quina de les dues classes
conté una referència cap a l'altra classe.
====

==== Operacions

Les **operacions** són les accions que pot realitzar una classe i
habitualment es corresponen amb els mètodes de la classe. Habitualment
no es mostren les operacions que simplement manipulen propietats, ja que
normalment es poden inferir.

`visibilitat nom (paràmetres) : retorn {etiquetes}`

`+ balanceOn (date : Date) : Money`

Els paràmetres:

`direcció nom: tipus = valor per defecte`

Les operacions i atributs estàtics es representen subratllats.

=== Generalitzacions

La relació de **generalització** és una relació entre dues classes que tenen
moltes similituds entre elles. Una d'elles, el supertipus, conté totes
les característiques comunes, mentre que l'altra, el subtipus, incorpora
algunes característiques pròpies.

El punt important aquí és que una instància del subtipus ha de ser també
una instància del supertipus.

Per exemple, la classe superior podria ser *PartCotxe*, i podria tenir una sèrie
de propietats, com el nom del fabricant, o el número de sèrie. Un *Neumàtic*
és una part d'un cotxe, i incorpora característiques noves, com la velocitat
que suporta i la durabilitat.

[link=uml/generalitzacio.puml]
image::imatges/generalitzacio.png[Exemple generalització]

A nivell d'implementació les generalitzacions es tradueixen en herència. El
principi més important a l'hora de decidir si cal aplicar herència o no en
la relació entre dues classes és el de **substituibilitat**: he de poder
passar un Neumàtic a qualsevol operació que necessiti una PartCotxe, i tot
ha de funcionar correctament.

L'herència és un mecanisme potent, però de vegades té efectes indesitjables.
No hi ha cap forma d'evitar que una subclasse heredi tots els atributs i
mètodes de la classe de la qual deriva, i en ocasions això no és el que es
desitja. La implementació d'interfícies i molts dels patrons de disseny estan
pensats com a mecanismes alternatius a l'herència.

==== Generalització contra classificació

Cal evitar la utilització de la generalització (herència) en casos en què la
relació no és *transitiva*. Entenem per relació transitiva el fet que si
la classe *C* deriva de la classe *B* i la classe *B* deriva de la classe
*A*, un objecte de tipus *C* és, a més de de tipus *B*, de tipus *A*.

Per exemple, un pastor alemany és un gos, i els gossos són animals. Així,
podem assegurar que un pastor alemany és un animal.

[link=uml/generalitzacio_versus_classificacio.puml]
image::imatges/generalitzacio_versus_classificacio.png[Generalització vàlida]

Considerem però el següent exemple: un pastor alemany és un gos, i els
gossos són una espècie. Per tant, un pastor alemany és una espècie.

[link=uml/generalitzacio_versus_classificacio.puml]
image::imatges/generalitzacio_versus_classificacio_001.png[Generalització incorrecta]

Evidentment, l'última conclusió és errònia. La relació entre gos i espècie
no és de generalització, sinó de classificació. En aquest tipus de situacions
aquestes relacions no es modelen bé utilitzant herència. En canvi, es
podria utilitzar una simple dependència entre gos i espècie per indicar
aquest fet:

[link=uml/generalitzacio_versus_classificacio.puml]
image::imatges/generalitzacio_versus_classificacio_002.png[Instanciació]

=== Anotacions

És habitual afegir anotacions als diagrames UML per tal de fer notar els
detalls que volem destacar del diagrama.

[link=uml/exemple_anotacio.puml]
image::imatges/exemple_anotacio.png[Exemple anotació]

=== Dependències

Existeix una **dependència** entre dos elements si els canvis en la
definició d'un dels elements poden causar canvis en l'altre. Entre
classes, les dependències poden existir bé perquè un classe envia un
missatge a una altra, bé perquè una classe en té una altra com a part de
les seves dades, o bé perquè una classe en menciona una altra com a
paràmetre d'una operació. Si una classe canvia la seva interfície,
qualsevol missatge que s'envia a aquesta classe pot deixar de ser vàlid.

[link=uml/exemple_dependencia.puml]
image::imatges/exemple_dependencia.png[Exemple dependència]

Els diagrames UML permeten mostrar dependències entre qualsevol parell
d'elements.

En un programa extens, no és viable mostrar totes les dependències
existents. Aleshores cal triar aquelles dependències que són rellevants a
allò que es vol mostrar.

Per veure les dependències a un nivell més alt, s'utilitza un *diagrama
de paquets*.

==== Agregació i composició

L'agregació i la composició són dos tipus de dependències molt habituals
que utilitzen els seus propis símbols en els diagrames.

Les dues relacions s'utilitzen per indicar una inclusió dels objectes
d'una classe en un objecte d'una altra classe.

L'**agregació** representa una relació de tipus *és-part-de*. Per exemple,
els socis formen part d'un club, o uns clients formen part de la cartera
de clients d'una empresa. L'agregació no és un concepte ben definit
i es confon fàcilment amb una *associació*, per aquest motiu hi ha qui
recomana no utilitzar aquest tipus de relació en els diagrames.

[link=uml/exemple_agregacio.puml]
image::imatges/exemple_agregacio.png[Exemple agregació]

La **composició** és un tipus de relació que implica exclusivitat. Per
exemple, un punt pot ser part d'un polígon o pot ser el centre d'un
cercle, però no les dues coses a la vegada; o una agenda inclou les seves
pàgines.

[link=uml/exemple_composicio.puml]
image::imatges/exemple_composicio.png[Exemple composició]

Per tal que una relació es consideri una composició cal que es compleixin
algunes condicions:

* Un dels objectes forma part de l'altre, de manera que si s'esborra
el segon, no té sentit que el primer segueixi existint.

* La multiplicitat de la classe inclosa respecte a la classe incloent
sempre és 0..1 o 1. És a dir, un objecte de la classe inclosa només pot
pertànyer a un objecte de la classe incloent.

=== Interfícies i classes abstractes

Les **interfícies** són un tipus de classe que no tenen implementació.
S'utilitza com a sistema per imposar un cert contracte, és a dir,
qualsevol classe que implementi una interfície ha d'implementar
obligatòriament (o deixar com abstractes) els seus mètodes. Amb això
s'aconsegueix que els objectes de qualsevol d'aquestes classes siguin
intercanviables entre ells.

De forma excepcional, a partir de la versió 8 del Java, existeixen
algunes interfícies que tenen codi per algun dels seus mètodes.

Les **classes abstractes** són classes de les quals no es poden
instanciar directament objectes. Per tal que un objecte sigui del tipus
d'una classe abstracta cal que es creï com a instància d'alguna de les
classes que deriven de la classe abstracta.

Un exemple clàssic de classe abstracta és la classe *Animal*. D'*Animal*
en poden derivar diverses classes: *Gos*, *Gat*, *Formiga*, etc. No té
sentit crear un animal que no tingui un tipus més concret, per això *Animal*
ha de ser abstracta. Quan creem un objecte de tipus *Gos*, aquest objecte
és també de tipus *Animal*.

Molt habitualment les classes abstractes inclouen alguns mètodes abstractes.
Els **mètodes abstractes** no tenen implementació. Les classes que deriven
d'una classe abstracte estan obligades a proporcionar el codi d'aquests
mètodes (o han de ser també abstractes).

Les *interfícies* es denoten amb la paraula clau *interface*, o amb
la lletra "I". Les classes i mètodes abstractes es mostren en itàliques,
o amb la paraula clau *abstract*.

Per dir que una classe implementa una interfície s'utilitza la mateixa
notació que per l'herència, però amb la línia puntejada. Per indicar que
una interfície amplia una altra interfície s'utilitza la mateixa notació
que per a l'herència de classes.

[link=uml/exemple_interficie.puml]
image::imatges/exemple_interficie.png[Exemple interfície]

=== Classes associatives

Les **classes associatives** ens permeten assignar propietats i operacions
a la relació entre dues classes.

Imaginem per exemple que de cada hora de classe volem guardar a quin
ordinador s'ha assegut cadascun dels alumnes.

Hi ha dues maneres d'indicar això:

1. Com una *classe associativa*:
+
[link=uml/exemple_classe_associativa.puml]
image::imatges/exemple_classe_associativa.png[Classe associativa]

2. Com una classe comuna:
+
[link=uml/exemple_classe_associativa.puml]
image::imatges/exemple_classe_associativa_001.png[Classe comuna]

La diferència aquí és subtil però important: indicant que *Posicio* és una
classe associativa estem deixant clar que només hi pot haver una *Posicio*
per a cada parella d'alumne-classe.

En canvi, en el segon cas, res impedeix que creem dues posicions diferents
per al mateix alumne a la mateixa hora de classe.

Així, una classe associativa afegeix una restricció que fa referència
a la relació entre les dues classes que enllaça.

Imaginem ara que modelem així la relació entre un professor i les matèries
que imparteix a un grup:

[link=uml/exemple_classe_associativa.puml]
image::imatges/exemple_classe_associativa_002.png[Falsa classe associativa]

Fixeu-vos que segons aquest diagrama no és possible que un professor
imparteixi dues matèries diferents al mateix grup. Si aquesta és una
possibilitat en el nostre model, la classe *Imparticio* no hauria de
ser una classe associativa.

La implementació d'una classe associativa es pot fer simplement com si
fos una classe habitual, amb mètodes que permetin accedir als objectes
associats.

== Diagrames de seqüència

Els **diagrames d'interacció** descriuen com un grup d'objectes col·laboren
en algun comportament. Dels diversos diagrames d'interacció que hi ha el
**diagrama de seqüència** és el més habitual.

Aquest diagrama mostra un conjunt d'objectes que exemplifiquen un
escenari concret i mostra els missatges que es passen aquests objectes
dins d'un cas d'ús concret. Els diagrames de seqüència són útils per
entendre la interacció entre diversos objectes d'un programa, però no
tant per representar complexitats algorísmiques concretes, com bucles i
condicionals.

Els objectes interactuen entre si amb l'enviament de missatges. La
recepció d'un missatge sol significar l'execució d'un mètode que
s'especifica en el missatge, i es torna actiu l'objecte mentre dura
l'execució del mètode.

[link=uml/exemple_diagrama_sequencia.puml]
image::imatges/exemple_diagrama_sequencia.png[Exemple de diagrama de seqüència]

Els elements són els següents:

* Una **línia de vida** representa un element connectable que participa
en una interacció enviant i rebent missatges. Una línia de vida representa
l'interval de temps en què existeix la instància o instàncies que formen
part de l’element connectable, des de la seva creació fins a la seva
destrucció, tot i que en general només durant una part o diverses
parts d’aquest interval participen en la interacció que conté la línia
de vida; aquestes parts s’anomenen activacions i representen els
intervals de temps durant els quals s’està executant alguna operació
que té com a objecte de context alguna de les seves instàncies.

* Un **missatge** és l'especificació de la comunicació entre objectes. Un
missatge es representa amb una fletxa de l'emissor cap al receptor;
diferents tipus de missatge es representen amb diferents tipus de
fletxa:

** Un *missatge asíncron* s'indica amb una fletxa de línia contínua i
punta oberta; un missatge és asíncron si l’objecte que l'emet no espera
la resposta a aquest missatge abans de continuar. Aquests missatges es
donen per exemple en l'activació de threads.

** Un *missatge síncron* es representa amb una fletxa de línia contínua
i punta plena; un missatge és síncron quan l'objecte que l'emet espera
la resposta a aquest missatge abans de continuar amb el seu treball.
Totes les crides a mètodes són missatges síncrons.

** Un *missatge de resposta d’un missatge síncron* s'indica amb una
fletxa de línia discontínua i punta plena.

** Un *missatge que provoca la creació d’un objecte* s'indica amb una
fletxa de línia discontínua i punta oberta; a més, l’extrem del
missatge coincideix amb el començament de la línia de vida del receptor.

[link=uml/tipus_fletxes_sequencia.puml]
image::imatges/tipus_fletxes_sequencia.png[Tipus de missatges]

Suposem que tenim una comanda i que invoquem un dels seus mètodes per
obtenir el seu preu. Per fer això, la comanda ha de mirar totes les
seves línies i determinar el preu de cadascuna, que es basa en les
regles per al preu del producte corresponent a la línia. Un cop s'ha fet
això per cada línia, la comanda ha de calcular un descompte general, que
es basa en les regles associades al client.

=== Exemple

El següent diagrama mostra una implementació d'aquest escenari:

[link=uml/exemple_comandes.puml]
image::imatges/exemple_comandes.png[Exemple comandes 1]

En el diagrama no es mostra que les crides a *getQuantity*,
*getProduct*, *getPricingDetails* i *calculateBasePrice* s'han de fer
per a cadascuna de les línies de la comanda, mentre que la crida a
*calculateDiscounts* només s'ha de fer una vegada. Encara que existeix
notació per indicar això en un diagrama de seqüència, no la farem, i ja
hem comentat que l'objectiu és entendre la interacció entre els
objectes, no els detalls de l'algorisme.

La següent figura representa una altra forma de resoldre el mateix
problema, però els objectes interaccionen d'una manera molt diferent. La
comanda demana a cada línia que calculi el seu propi preu. La línia
delega aquest càlcul al producte. De forma similar, per calcular el
descompte, la comanda invoca un mètode en el client. Com que el client
necessita informació de la comanda per poder-ho fer, el client fa una
crida reentrant (*getBaseValue*) a la comanda per obtenir les dades.

La primera cosa a notar sobre aquests dos diagrames és la claredat amb
què mostren la diferent forma d'interaccionar dels participants. Aquesta
és la gran fortalesa dels diagrames de seqüència.

El segon punt a notar és la clara diferència d'estils entre les dues
solucions. En el primer cas s'utilitza una forma de **control
centralitzat**, en què un participant fa tot el procediment i els altres
participants es limiten a proporcionar les dades necessàries. La segona
solució utilitza un **control distribuït**, en què el procés es divideix
entre diversos participants, i cadascun d'ells fa una petita part de
l'algorisme.

[link=uml/exemple_comandes.puml]
image::imatges/exemple_comandes_001.png[Exemple comandes 2]

Els dos estils tenen els seus avantatges i inconvenients. El control
centralitzat és més senzill, perquè tot el procés es realitza en un sol
lloc, i és més fàcil d'entendre per aquells que són nous en la
programació orientada a objectes. Amb el control distribuït, en canvi,
es té la sensació de perseguir els objectes, intentant trobar on és el
programa.

Tot i això, un dels objectius principals d'un bon disseny és localitzar
els efectes del canvi. Les dades i el comportament que utilitza aquestes
dades normalment canvien a la vegada. Així que posar les dades i el
comportament que les utilitza junts en un sol lloc és la primera norma
del disseny orientat a objectes.

A més, amb el control distribuït, creem més oportunitats per utilitzar
el polimorfisme en comptes d'utilitzar la lògica condicional. Si els
algorismes per determinar el preu d'un producte són diferents per
diferents tipus de producte, el mecanisme del control distribuït ens
permet utilitzar una subclasse de producte per gestionar aquestes
variacions.

== Casos d'ús

Els **casos d'ús** s'utilitzen per entendre els requeriments funcionals d'un
sistema. Un cas d'ús descriu com un usuari interacciona amb el sistema,
habitualment amb un objectiu concret. Per exemple, un usuari vol fer una
compra a través de la nostra pàgina web de venta online.

Un cas d'ús es desgrana en una sèrie de passos. Per exemple, en el cas de
la compra online, l'usuari haurà d'afegir una sèrie d'ítems al carretó,
indicar on se li han d'enviar i el mètode de pagament, i realitzar aquest
pagament. A més, aquest mateix cas d'ús inclou variacions en els possibles
escenaris que es poden donar: si l'usuari ja està registrat o no al sistema,
si el pagament es pot validar immediatament o es produeix un error, etc.

El cas d'ús agrupa tots aquests possibles escenaris i passos, perquè el seu
objectiu es entendre quin tipus d'interaccions hi haurà en el nostre sistema
i no considerar totes les possibles situacions que es poden donar.

Els **diagrames de casos d'ús** s'utilitzen per modelar els casos d'ús i
s'acostumen a realitzar en una fase inicial del projecte. Com que la seva
interpretació és senzilla, els diagrames de casos són útils per acordar amb
el client quines són les funcionalitats principals del programa. En aquest
sentit, cal evitar detallar massa els passos de cada interacció.

Els elements que actuen amb el nostre sistema s'anomenen **actors**. Hem parlat
d'usuaris, que són els actors més habituals, però n'hi ha d'altres, com
treballadors de l'empresa, administradors, o fins i tot altres sistemes
informàtics externs al nostre. Així, un actor es correspon amb un *rol* que un
usuari pren en el sistema (podria ser que el mateix usuari tingués diversos
rols, i a efectes del diagrama es representaria com a dos actors diferents).

=== Contingut dels casos d'ús

Tot i l'existència dels diagrames de casos d'ús en UML, la descripció completa
del cas d'ús acostuma a ser molt més útil que el propi diagrama.

Per descriure un cas d'ús, un mètode habitual és partir d'un dels escenaris,
el que considerem l'**escenari exitós principal**. Aquest escenari es descriu
amb una sèrie de passos numerats, que indiquen cadascuna de les fases del
procés que es desenvolupa entre els actors i el sistema fins a assolir
l'objectiu del cas d'ús.

Cada un d'aquests passos ha de ser una interacció entre un actor i el sistema,
s'ha de poder expressar clarament en una sentència, i ha de deixar clar qui
està realitzant l'acció.

A continuació es descriuen les diverses variants que es poden donar de
l'escenari principal.

En alguns casos, un cas d'ús pot incloure altres casos. Per exemple, en una
botiga virtual, el cas d'ús de realitzar una compra inclou en un dels seus
passos inicials el cas d'ús de fer una cerca en el catàleg de la tenda.

Altres informacions que pot ser útil incloure a la descripció textual d'un
cas d'ús són:

- **Prerequisits**: coses que el sistema ha d'assegurar que són certes abans de
poder procedir amb el cas d'ús.
- **Garanties**: coses que el sistema ha de garantir que siguin certes un cop
el cas d'ús s'hagi finalitzat.
- **Disparador** (*trigger*): l'esdeveniment que provoca l'inici del cas d'ús.

=== Exemple de cas d'ús: compra a través de la web

**Resum**: El comprador sol·licita alguns productes a través de la nostra web.
Cal enviar-li els objectes que ha comprat i cal gestionar el pagament.

**Prerequisits**: tenim les dades del comprador.

**Garanties**: s'ha enviat la comanda al transportista. El comprador ha pagat.

**Disparador**: el comprador es connecta a la botiga virtual.

==== Escenari exitós principal

1. El comprador es valida a la pàgina web.
2. El comprador cerca els productes al catàleg i els afegeix al carretó.
3. El comprador revisa el carretó i confirma la compra.
4. La web mostra un resum de la compra, preus, opcions d'enviament, etc.
5. El comprador tria una opció d'enviament.
6. El comprador tria un sistema de pagament.
7. El sistema redirigeix el pagament a l'entitat que correspongui.
8. L'entitat bancària confirma el pagament.
9. El sistema enregistra la compra a magatzem, per tal de preparar els productes.
10. El sistema enregistra la compra al transportista.
11. El sistema envia un email de confirmació amb les dades de la compra.
12. El sistema mostra un resum amb les dades de la compra i permet imprimir
la factura.

==== Variacions

3a. Algun dels productes no es pot enviar per falta d'estoc.
3a1. Cal modificar la comanda i demanar confirmació.

5a. El comprador vol enviar els productes a una adreça que no és la seva
adreça habitual.
5a1. Cal oferir la possibilitat d'introduir una nova adreça.

6a. El comprador vol pagar amb una targeta nova.
6a1. Cal oferir la possibilitat d'introduir les dades d'una nova targeta.

7a. No es pot contactar amb l'empresa de pagament.
7a1. Tornar a provar o passar a escenari 8a1.

8a. L'empresa de pagament no accepta el pagament.
8a1. Cal donar l'opció de triar un altre sistema de pagament.

9a. No es pot contactar amb el transportista.
9a1. Cal registrar la incidència per tal de procedir a l'enviament més tard.

==== Sub-variacions

1. El comprador pot pagar a través de targeta de crèdit, targeta de dèbit o
paypal.

==== Casos d'ús subordinats

- Cerca al catàleg
- Gestió del carretó
- Pagament per cadascun dels sistemes

==== Actors

**Actor principal**: client

**Actors secundaris**: empresa de targetes de crèdit o pagament online,
empresa de missatgeria.

==== Casos d'ús relacionats

- Anul·lació de compres.
- Reclamació i retorn de productes.

==== Diagrama de casos d'ús

En aquest diagrama apareix aquest cas d'ús i els seus casos d'ús relacionats
(sense desenvolupar):

[link=uml/exemple_cas_dus.puml]
image::imatges/exemple_cas_dus.png[Exemple diagrama casos d'ús]
